--extra-index-url https://download.pytorch.org/whl/cpu
torch==1.13.0+cpu
torchvision
transformers
datasets
optimum[openvino,nncf]
evaluate
scikit-learn
